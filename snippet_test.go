package main

import (
	"testing"
)

func TestTitle(t *testing.T) {
	testCases := []struct {
		content string
		output  string
	}{
		{"title", "title"},
		{"", ""},
		{
			content: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
			output:  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
		},
		{"teststring\ntest!", "teststring"},
	}

	for _, tc := range testCases {
		if title(&tc.content) != tc.output {
			t.Fail()
		}
	}
}
